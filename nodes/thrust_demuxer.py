#!/usr/bin/env python
'''
On the robot, we use a single message for all thrusters.
Using Project Dave / uuv_simulator, each thruster plugin gets its own topic.
So, split them all out!
'''
import os

import rospy

from greensea_msgs.msg import ThrusterCmd
from uuv_gazebo_ros_plugins_msgs.msg import FloatStamped


class ThrustDemuxer:
    def __init__(self, namespace='raven'):
        self.gain = 1000.0
        # Joystick to thruster i.d. mapping
        self.publishers = [rospy.Publisher('/%s/thrusters/%d/input'%(namespace,ii+1), FloatStamped, queue_size=1) for ii in range(11)]

        self.cmd_sub = rospy.Subscriber('cmd', ThrusterCmd, self.cmd_callback)

    def cmd_callback(self, cmd):
        msg = FloatStamped()
        for idx, val in enumerate(cmd.commands):
            msg.data = val * 10
            self.publishers[idx].publish(msg)

if __name__ == '__main__':
    # Start the node
    node_name = os.path.splitext(os.path.basename(__file__))[0]
    rospy.init_node(node_name)
    rospy.loginfo('Starting [%s] node' % node_name)

    # Get params
    ns = 'raven'
    if rospy.has_param('~namespace'):
        ns = rospy.get_param('~namespace')

    demux = ThrustDemuxer(namespace = ns)
    rospy.spin()
    rospy.loginfo('Shutting down [%s] node' % node_name)
