#! /usr/bin/env python3

import numpy as np
import rospy

from geometry_msgs.msg import PoseStamped, WrenchStamped
from greensea_msgs.msg import TiltCmd
from sensor_msgs.msg import Joy
from tf_conversions import transformations
import tf2_geometry_msgs
import tf2_ros

class RavenTeleop(object):
    """
    Simple node that maps joystick input to commanded wrenches.
    Only publish wrench if at least one of the joystick's buttons is in a
    non-default position.

    Top-left button is enable.
    - This is how the use can specify "no wrench")
    - In the current implementation, ANY button is actually enable; it may
      become more specific if more buttons are used for non-wrench commands.

    All commands are specified in vehicle frame:
    * Left stick is fwd/back left/right
    * Right stick is up/down yaw

    For now, no attempt has been made to also support any other joystick
    functionality. In the future, we may want to extend this to tilt control.
    """
    def __init__(self):
        self.joy_sub = rospy.Subscriber("joy", Joy, self.joy_callback)
        self.wrench_pub = rospy.Publisher("joy_wrench", WrenchStamped, queue_size=1)
        self.pose_pub = rospy.Publisher("joy_pose", PoseStamped, queue_size=1)
        self.tilt_pub = rospy.Publisher("tilt_dir", TiltCmd, queue_size=1)
        self.tf_buffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tf_buffer)

        self.robot_frame = rospy.get_param("~robot_frame", "base_link")
        self.cmd_frame = rospy.get_param("~cmd_frame", "world_gazebo")

        rospy.loginfo("Starting RavenTeleop node with robot_frame {}"
                      .format(self.robot_frame))

        self.x_scale = rospy.get_param("~x_scale", 1.0)
        self.y_scale = rospy.get_param("~y_scale", 1.0)
        self.z_scale = rospy.get_param("~z_scale", 1.0)
        self.yaw_scale = rospy.get_param("~yaw_scale", 1.0)

    def joy_callback(self, joy_msg):
        """
        Map buttons to force/torque, using a vehicle-oriented Fwd, Stbd, Down
        frame.
        """
        if not self.is_active(joy_msg):
            return

        # Joystick commands are published both in the form of a wrench and
        # a pose in the robot's frame the corresponds to a small displacement
        # along the appropriate axes. (roughly equivalent to velocity control)
        # For now, only the carrot is used.
        self.publish_wrench(joy_msg)
        self.publish_carrot(joy_msg)
        # Y button is buttons[3] and should tilt down
        # A button is buttons[0] and should tilt up
        self.publish_tilt(joy_msg)

    def publish_tilt(self, joy_msg):
        up = joy_msg.buttons[0]
        down = joy_msg.buttons[3]

        # Do nothing if neither button pressed
        if not up and not down:
            return

        # If both buttons pressed, don't guess which one the user meant.
        if up and down:
            err_msg = "raven_teleop: Tilt UP and DOWN both pressed."
            rospy.logerr(err_msg)
            return

        tilt_msg = TiltCmd()
        if down:
            tilt_msg.tilt_down = True
        else:
            tilt_msg.tilt_up = True
        self.tilt_pub.publish(tilt_msg)


    def publish_carrot(self, joy_msg):
        """
        Publish goal pose, assuming that joystick commands correspond to a
        step along the different axes in vehicle frame.
        """
        pose_msg = PoseStamped()
        pose_msg.header.stamp = rospy.Time.now()
        step_size = 0.1  # TODO: less hacky step size?
        pose_msg.header.frame_id = self.robot_frame
        pose_msg.pose.position.x = step_size * self.x_scale * joy_msg.axes[1]
        pose_msg.pose.position.y = step_size * -1.0 * self.y_scale * joy_msg.axes[0]
        pose_msg.pose.position.z = step_size * self.z_scale * joy_msg.axes[4]
        dyaw =  -1.0 * step_size * self.yaw_scale * joy_msg.axes[3]
        quat = transformations.quaternion_from_euler(0, 0, dyaw)
        pose_msg.pose.orientation.x = quat[0]
        pose_msg.pose.orientation.y = quat[1]
        pose_msg.pose.orientation.z = quat[2]
        pose_msg.pose.orientation.w = quat[3]

        try:
            transform = self.tf_buffer.lookup_transform(self.cmd_frame,
                    pose_msg.header.frame_id,
                    rospy.Time(0),
                    rospy.Duration(0.1))
            pose_msg = tf2_geometry_msgs.do_transform_pose(pose_msg, transform)
            self.pose_pub.publish(pose_msg)
        except Exception as ex:
            rospy.logerr("Exception while trying to transform from {} to {}"
                    .format(pose_msg.header.frame_id, self.cmd_frame))
            rospy.logerr(ex)

    def publish_wrench(self, joy_msg):
        """
        Publish wrench message, assuming that the sticks correspond
        to effort along the different axes.
        """
        wrench_msg = WrenchStamped()
        wrench_msg.header.stamp = rospy.Time.now()
        wrench_msg.header.frame_id = self.robot_frame

        # On both sticks, forward and left are positive. So, flip L/R sign
        # to match vehicle axes.
        # Left stick, fwd/back
        wrench_msg.wrench.force.x = self.x_scale * joy_msg.axes[1]
        # Left stick, left/right
        wrench_msg.wrench.force.y = -1.0 * self.y_scale * joy_msg.axes[0]
        # Right stick, fwd/back
        wrench_msg.wrench.force.z = self.z_scale * joy_msg.axes[4]
        # Right stick, left/right
        wrench_msg.wrench.torque.z = -1.0 * self.yaw_scale * joy_msg.axes[3]

        self.wrench_pub.publish(wrench_msg)

    def is_active(self, joy_msg):
        """
        Determine whether any of the buttons/sticks are in a
        non-neutral position.
        """
        if np.any(joy_msg.buttons):
            return True

        # The top buttons that are axes seem to have a bug.
        # https://github.com/ros-drivers/joystick_drivers/issues/155
        # When the joystick is initialized, they report 0.
        # However, after they've been touched, neutral is 1.0 with a
        # range of [-1.0, 1.0]. I think the right fix would be for them
        # to range from 0 to 1, but at this point I suspect that would break
        # a lot of code.

        # First, test for as-initialized state; if all values are 0.0, it's
        # almost certainly inactive. Technically, somebody could be holding
        # One of the buttons exactly halfway down, but that's unlikely enough
        # to ignore.
        if not np.any(joy_msg.axes):
            return False

        # Second, test for the after-usage state, where axes 2 and 5 will
        # report 1.0 when at rest.
        weird_axes = {2,5}
        nonzero_axes = set(np.where(joy_msg.axes)[0])
        # If any normal axes are nonzero, joystick is active
        if len(nonzero_axes.difference(weird_axes)) > 0:
            return True
        # If any weird axes are nonzero AND not 1.0, joystick is active
        for ax in nonzero_axes.intersection(weird_axes):
            if joy_msg.axes[ax] != 1.0:
                return True

        return False



if __name__ == "__main__":
    rospy.init_node("raven_teleop")
    teleop = RavenTeleop()
    rospy.spin()

