#! /usr/bin/env python3
"""
Tool for testing/debugging.
Publish 4 waypoints in a box.
"""

import numpy as np
import rospy
import tf_conversions

from geometry_msgs.msg import Pose, PoseArray

if __name__ == "__main__":
    rospy.init_node("pub_box_waypoints")

    frame = rospy.get_param("~frame", "world")
    x_offset = rospy.get_param("~x_offset", 0.0)
    y_offset = rospy.get_param("~y_offset", 0.0)

    pub = rospy.Publisher("waypoints", PoseArray, queue_size=1, latch=True)

    msg = PoseArray()
    msg.header.stamp = rospy.Time.now()
    msg.header.frame_id = frame

    zz = 85.0  # 5 meters deep
    for xx, yy, yaw in [[10, 10, 5*np.pi/4], [0, 10, 7*np.pi/4], [0, 0, np.pi/4], [10, 0, 3*np.pi/4]]:
        pose = Pose()
        pose.position.x = xx + x_offset
        pose.position.y = yy + y_offset
        pose.position.z = zz
        quat = tf_conversions.transformations.quaternion_from_euler(0, 0, yaw)
        pose.orientation.x = quat[0]
        pose.orientation.y = quat[1]
        pose.orientation.z = quat[2]
        pose.orientation.w = quat[3]
        msg.poses.append(pose)

    pub.publish(msg)

    rospy.spin()

