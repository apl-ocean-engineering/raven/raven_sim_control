#! /usr/bin/env python3
"""
Given a set of waypoints and current vehicle state, calculate the "carrot"
that the vehicle should be using for its control input.

The carrot will be in the direction of the next waypoint, but with a
fixed maximum step size. Position and orientation steps are calculated
separately, and the orientation for the waypoint is taken to correspond
the whole segment before it.
"""

import numpy as np
import rospy
import tf
from tf_conversions import transformations

from geometry_msgs.msg import Pose, PoseArray, PoseStamped
from nav_msgs.msg import Odometry


class CarrotPlanner(object):
    """
    Compute the next target position for the vehicle.
    * With very small step sizes, this can be paired with the
      teleporting plugin.
    * Larger step sizes are useful as the reference for a PID controller
      wrapped around position goals.
    """
    def __init__(self):
        # index into current path message
        self.path_index = None
        self.path_msg = None

        # Vehicle's frame
        # For now, just doing everything in the odom frame
        self.cmd_frame = rospy.get_param("~cmd_frame", "base_link")

        # Maximum step size for the next published goal.
        self.step_size_meters = rospy.get_param("~step_size_meters", 5.0);
        self.step_size_radians = rospy.get_param("~step_size_radians",
                                                 2*np.pi/3);

        # How close the vehicle must get to the current waypoint before
        # considering it achieved and attempting to get to the next one.
        self.waypoint_radius_meters = rospy.get_param("~waypoint_radius_meters", 0.25)
        self.waypoint_radius_radians = rospy.get_param("~waypoint_radius_radians", np.radians(10))

        self.path_sub = rospy.Subscriber("/waypoints", PoseArray, self.handle_path)

        # Use odometry rather than tf so this node can be callback driven
        self.odom_sub = rospy.Subscriber("/odom", Odometry, self.handle_odom)

        self.pose_pub = rospy.Publisher("/cmd_pose", PoseStamped, queue_size=1)

        self.listener = tf.TransformListener()

    def handle_path(self, path_msg):
        """
        Update the current path.
        """
        # TODO: consider checking if the two path messages are identical
        #   and only updating if they're actually different
        if len(path_msg.poses) > 0:
            self.path_index = 0
        else:
            rospy.logwarn("Received path with no waypoints: {}".format(path_msg))
            self.path_index = None
        self.path_msg = path_msg

    def print_pose(self, pose, message):
        rospy.logwarn("{}: {:0.2f}, {:0.2f}, {:0.2f}".format(message, pose.position.x, pose.position.y, pose.position.z))

    def handle_odom(self, nav_msg):
        """
        Every odometry message triggers an update of the carrot.

        This is a bit ugly because it deals with three frames:
        * Frame the path is represented in; this is what we use
          for all calculations.
        * Frame that odometry is reported in
        * Frame that commands should be published in
        """
        if self.path_msg is None or self.path_index is None:
            # TODO: carrot should be vehicle's present position
            # rospy.logwarn("Cannot publish carrot without a path.")
            return

        # Convert from Odometry to PoseStamped to we can use tf to transform
        nav_pose = PoseStamped()
        nav_pose.header = nav_msg.header
        nav_pose.pose = nav_msg.pose.pose
        path_frame = self.path_msg.header.frame_id
        if path_frame != nav_msg.header.frame_id:
            try:
                nav_pose = self.listener.transformPose(path_frame, nav_pose)
            except Exception as ex:
                rospy.logerr("Could not transform frames!")
                rospy.logerr(ex)
                return


        wpt = self.path_msg.poses[self.path_index]

        # Calculate the x/y/z deltas
        wpt_point = np.array([wpt.position.x, wpt.position.y, wpt.position.z])
        pos = nav_pose.pose.position
        pose_point = np.array([pos.x, pos.y, pos.z])
        dpose = wpt_point - pose_point

        # Calculate the Yaw delta
        wpt_quat = np.array([wpt.orientation.x, wpt.orientation.y,
                             wpt.orientation.z, wpt.orientation.w])
        _, _, wpt_yaw = transformations.euler_from_quaternion(wpt_quat)

        quat = nav_pose.pose.orientation
        pose_quat = np.array([quat.x, quat.y, quat.z, quat.w])
        _, _, pose_yaw = transformations.euler_from_quaternion(pose_quat)

        # rotation from qa -> qb = q1.inverse() * qb
        inv = transformations.quaternion_inverse(pose_quat)
        dquat = transformations.quaternion_multiply(inv, wpt_quat)

        _, _, dyaw = transformations.euler_from_quaternion(dquat)

        # If we're within radius of the current waypoint, increment if necessary
        pose_close = np.sqrt(dpose@dpose) < self.step_size_meters
        # NB: We only care about yaw orientation since we don't have pitch/roll
        #     control. However, using quaternions for angle calculations means
        #     I don't have to mess around with angles wrapping.
        #quat_close = 2*np.arccos(dquat[3]) < self.step_size_radians
        yaw_close = np.abs(dyaw) < self.step_size_radians
        if pose_close and yaw_close:
            self.path_index = min(self.path_index+1, len(self.path_msg.poses)-1)
            # TODO: Should recalculate the deltas ... instead, the waypoint
            #       increments on the next update.

        step_size = min(self.step_size_meters, np.linalg.norm(dpose))
        cmd_point = pose_point + step_size * dpose / np.linalg.norm(dpose)
        step_size = min(self.step_size_radians, abs(dyaw))
        cmd_yaw = pose_yaw + step_size * np.sign(dyaw)
        cmd_quat = transformations.quaternion_from_euler(0, 0, cmd_yaw)

        carrot = PoseStamped()
        carrot.header.stamp = rospy.Time.now()
        carrot.header.frame_id = path_frame
        carrot.pose.position.x = cmd_point[0]
        carrot.pose.position.y = cmd_point[1]
        carrot.pose.position.z = cmd_point[2]
        carrot.pose.orientation.x = cmd_quat[0]
        carrot.pose.orientation.y = cmd_quat[1]
        carrot.pose.orientation.z = cmd_quat[2]
        carrot.pose.orientation.w = cmd_quat[3]

        if carrot.header.frame_id != self.cmd_frame:
            carrot = self.listener.transformPose(self.cmd_frame, carrot)

        self.pose_pub.publish(carrot)


if __name__ == "__main__":
    rospy.init_node("carrot_planner")
    cp = CarrotPlanner()
    rospy.spin()
