#! /usr/bin/env python3
"""
Tool for testing/debugging.
Publish 4 waypoints in a box.
"""

import numpy as np
import pyproj
import rospy
import sys
import tf_conversions

from geometry_msgs.msg import Pose, PoseArray

import lcm
from gss import nav_solution_t

class PathPublisher(object):
    def __init__(self):
        self.proj = None

        # I really don't like the ROS path publisher having a dependency on LCM,
        # but since we want to be able to input geographic coords, the ground truth
        # for the conversion lives in OPENINS_NAV_SOLUTION.initial_lonlat_fix
        self.lc = lcm.LCM()
        self.projection_initialized = False
        self.nav_sub = self.lc.subscribe("OPENINS_NAV_SOLUTION", self.handle_nav)

        self.waypoints_sent = False
        self.waypoints_pub = rospy.Publisher("/raven/path", PoseArray, queue_size=1, latch=True)
        self.pub_timer = rospy.Timer(rospy.Duration(1.0), self.publish_waypoints)


    def initialize_projection(self, lon, lat):
        """
        Initialize the transverse mercator projection that is used within ROS.
        """
        proj_string = "+proj=tmerc +lat_0={} +lon_0={}".format(lat, lon)
        self.proj = pyproj.Proj(proj_string)
        self.projection_initialized = True

    def local_xy_from_lonlat(self, lon, lat):
        easting, northing = self.proj(lon, lat)
        # NED coordinate frame
        return northing, easting

    def lonlat_from_local_xy(self, xx, yy):
        lon, lat = self.proj(yy, xx, inverse=True)
        return lon, lat

    # If I need this again, figure out a way to generalize this into an importable class
    def handle_nav(self, _channel, data):
        print("handle_nav!")
        msg = nav_solution_t.decode(data)
        if not self.projection_initialized:
            init_lon, init_lat = msg.initial_lonlat_fix
            self.initialize_projection(init_lon, init_lat)
            self.lc.unsubscribe(self.nav_sub)

    def publish_waypoints(self, _event):
        print("handle waypoints!")
        # This is beyond ugly, but I need to periodically call LCM handle as well.
        self.lc.handle()

        # Can't create waypoints if our projection isn't initialized
        if not self.projection_initialized:
            return

        # Want a delay between publication and quitting to make sure messages are sent out.
        if self.waypoints_sent:
            rospy.signal_shutdown('done publishing')

        # TODO: Some of this could become parameters...

        # Depths at which to circle
        depths = [2, 4, 6]

        # Center coordinate to circle
        # TODO: It's easiest to recover lon, lat from Workspace, but ROS expects them in xyz
        #    And, only the translator knows
        # lon, lat = -122.3199582, 47.6536439
        # lon, lat = -122.2258835, 47.6483455
        # lon, lat = -122.2258673, 47.6483412
        # lon, lat = -122.2258684, 47.6483608
        lon, lat = -122.2258695, 47.6483791
        center_xy = self.local_xy_from_lonlat(lon, lat)

        standoff = 5  # Radius at which to circle
        start_heading = 45  # In degrees; robot heading to look at center point
        end_heading = 270
        delta_heading = 20  # Change in heading between successive waypoints

        poses = self.get_poses(center_xy, standoff, depths, start_heading, end_heading, delta_heading)

        msg = PoseArray()
        msg.header.stamp = rospy.Time.now()
        # frame = rospy.get_param("~frame", "gss_origin")
        msg.header.frame_id = "gss_origin"
        msg.poses = poses
        self.waypoints_pub.publish(msg)
        rospy.loginfo("Published {} waypoints".format(len(poses)))
        self.waypoints_sent = True

    def get_poses(self, center_xy, standoff, depths, start_heading, end_heading, delta_heading):
        poses = []
        for (idx, depth) in enumerate(depths):
            # Alternate direction of arc at each depth to avoid an awkward transit
            if idx % 2 == 0:
                theta1 = start_heading
                theta2 = end_heading
                dtheta = delta_heading
            else:
                theta1 = end_heading
                theta2 = start_heading
                dtheta = -1 * delta_heading

            # Step along arc, adding waypoints
            cmd_heading = theta1
            while np.abs(theta2 - cmd_heading) > np.abs(delta_heading):
                yaw = np.radians(cmd_heading)
                pose = Pose()
                pose.position.x = center_xy[0] - standoff * np.cos(yaw)
                pose.position.y = center_xy[1] - standoff * np.sin(yaw)
                pose.position.z = depth
                quat = tf_conversions.transformations.quaternion_from_euler(0, 0, yaw)
                pose.orientation.x = quat[0]
                pose.orientation.y = quat[1]
                pose.orientation.z = quat[2]
                pose.orientation.w = quat[3]
                poses.append(pose)

                cmd_heading += dtheta
                cmd_heading = cmd_heading % 360
        return poses

if __name__ == "__main__":
    rospy.init_node("pub_arc_waypoints")
    pp = PathPublisher()
    rospy.spin()

