#! /usr/bin/env python3
"""
Provide minimal switching behavior between joystick and autonomous control.

Joystick always immediately takes control.
After joystick input ceases, "hold station" (don't send any commands through)
When a new path is published, transition from PENDING -> AUTONOMOUS

TODO: When I figure out how Greensea resumes autonomous control after

NB: Truly duplicating the Greensea behavior would require the switching
behavior to happen farther upstream, in whatever node is choosing the
current waypoint. (In order to station keep at the current location after
joystick input ceases.)
However, since the sim control pipeline is in flux, it was easiest to
implement it here and just don't send any commands until autonomous control
is reenabled.
"""

import rospy

from geometry_msgs.msg import PoseArray, PoseStamped, WrenchStamped
from std_msgs.msg import String

class SimManager(object):
    def __init__(self):
        self.status = "PENDING"
        self.last_joystick = None
        self.joystick_timeout = rospy.Duration(1.0)

        joystick_topic = "human_cmd"
        self.joystick_sub = rospy.Subscriber(joystick_topic, PoseStamped,
                                             self.handle_joystick)
        controller_topic = "robot_cmd"
        self.controler_sub = rospy.Subscriber(controller_topic, PoseStamped,
                                              self.handle_controller)
        waypoints_topic = "waypoints"
        self.waypoints_sub = rospy.Subscriber(waypoints_topic, PoseArray,
                                              self.handle_waypoints)

        output_topic = "output_cmd"
        self.cmd_pub = rospy.Publisher(output_topic, PoseStamped, queue_size=1)

        status_topic = "status"
        self.status_pub = rospy.Publisher(status_topic, String, queue_size=1)

        self.timer = rospy.Timer(rospy.Duration(0.1), self.timer_callback)

    def timer_callback(self, _):
        msg = String()
        msg.data = self.status
        self.status_pub.publish(msg)

        if self.status == "JOYSTICK":
            dt = rospy.Time.now() - self.last_joystick
            if dt > self.joystick_timeout:
                self.status = "PENDING"

    def handle_waypoints(self, _):
        if self.status == "PENDING":
            self.status = "WAYPOINTS"

    def handle_joystick(self, joy_msg):
        self.status = "JOYSTICK"
        self.last_joystick = rospy.Time.now()
        self.cmd_pub.publish(joy_msg)

    def handle_controller(self, msg):
        if self.status == "WAYPOINTS":
            self.cmd_pub.publish(msg)

if __name__ == "__main__":
    rospy.init_node("sim_manager")
    sm = SimManager()
    rospy.spin()
