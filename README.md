Paackage for various nodes that are used primarily in a ROS-only simulation
(In order to integrate with Gazebo, we have to replicate some of GSS's
 functionality.)

None of these are meant to be particularly high performance or tuned -- the
goal is simply to have a simulated robot that we can drive around using the
same interface as in the real system.
